﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    // public parameters
    public float speed = 1f;
    public float maxSpeed = 1000f;
    public float kickPower = 500f;
    public GameObject ball;

    // components
    private Rigidbody2D rb2d;
    private Collider2D collider;
    private SpriteRenderer spriteRenderer;

    // mechanics fields
    private float moveX;
    private float moveY;
    private bool kicking;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
    }

    // physics loop
    void FixedUpdate()
    {
        handleMovement();
        handleKick();
    }

    private void handleKick()
    {
        if (kicking && ball.GetComponent<Collider2D>().IsTouching(collider))
        {
            // get kick vector using ball and player
            Vector2 kickVector = ball.transform.position - transform.position;

            // normalize vector and amplify using kick power
            kickVector = (kickVector / kickVector.magnitude) * kickPower;
            ball.GetComponent<Rigidbody2D>().AddForce(kickVector);
        }
    }

    private void handleMovement()
    {
        Vector2 moveVector = new Vector2(moveX, moveY);
        rb2d.AddForce(moveVector);
        if (rb2d.velocity.magnitude > maxSpeed)
        {
            rb2d.velocity = rb2d.velocity.normalized * maxSpeed;
        }
    }

    // render/input loop
    void Update () {
        moveX = Input.GetAxis("Horizontal") * speed;
        moveY = Input.GetAxis("Vertical") * speed;
        
        if (moveX > 0.1)
        {
            spriteRenderer.flipX = false;
        }
        if (moveX < -0.1)
        {
            spriteRenderer.flipX = true;
        }

        kicking = Input.GetKey(KeyCode.Space);
    }
}
